<?php

/**
 * @return array<string>
 */
function get_environments(string $token, string $project_id): array {
    $opts = array(
        'http'=>array(
            'method'=>"GET",
            'header'=>"PRIVATE-TOKEN: $token\r\n"
        )
    );

    $url = "https://gitlab.cern.ch/api/v4/projects/$project_id/environments";

    $context = stream_context_create($opts);
    $json = file_get_contents($url, false, $context);

    $envs = [];
    if ($json) {
        $envs_info = json_decode($json, true);
        if ($envs_info) {
            foreach($envs_info as $env) {
                array_push($envs, $env['slug']);
            }
        }
    }
    return $envs;
}

/**
 * @return array<string>
 */
function get_flags(string $token, string $project_id): array {
    $opts = array(
        'http'=>array(
          'method'=>"GET",
          'header'=>"PRIVATE-TOKEN: $token\r\n"
        )
    );

    $url = "https://gitlab.cern.ch/api/v4/projects/$project_id/feature_flags";

    $context = stream_context_create($opts);
    $json = file_get_contents($url, false, $context);

    $flags = [];
    if ($json) {
        $flags_info = json_decode($json, true);
        if ($flags_info) {
            foreach($flags_info as $flag_info) {
                $name = $flag_info['name'];
                $env = $flag_info['strategies'][0]['scopes'][0]['environment_scope'];
                $prefix = $env.'-';
                if (substr($name, 0, strlen($prefix)) === $prefix) {
                    $flag = substr($name, strlen($prefix));
                    if (!in_array($flag, $flags)) {
                        array_push($flags, $flag);
                    }
                }
            }
        }
    }
    return $flags;
}

$token = $ff_details["token"];

$project_id = $ff_details["project_id"];

$envs = get_environments($token, $project_id);

$flags = get_flags($token, $project_id);

$data = [];
$current_env = $ff_details["env"];

$row = [];
$row['flag'] = '_current_env_';
foreach($envs as $env) {
    $row[$env] = $env == $current_env;
}
array_push($data, $row);

foreach($flags as $flag) {
    $row = [];
    $row['flag'] = $flag;
    foreach($envs as $env) {
        $row[$env] = flag_is_enabled($flag, $env);
    }
    array_push($data, $row);
}

header('Content-Type: application/json');
echo json_encode( $data );
