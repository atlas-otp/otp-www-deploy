#!/bin/sh

dir="$(dirname "$(readlink -f $0)")"
output=$(php-cgi -f $dir/OvervaluedShiftsData.php count)
count=$?
if [ $count -ne 0 ]
then
  echo "There are $count overvalued shifts in point1, check https://otp-atlas.web.cern.ch/otp-atlas/checks/OvervaluedShifts.php" | mailx -s "Overvalued Shifts in Point1" -c duns@cern.ch duns@cern.ch
fi
#echo $output
