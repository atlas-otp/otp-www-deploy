#!/bin/sh

dir="$(dirname "$(readlink -f $0)")"
output=$(php-cgi -f $dir/CategoryUsageData.php count)
count=$?
if [ $count -ne 0 ]
then
  echo "There are $count tasks in OTP using category 1 or 2, check https://otp-atlas.web.cern.ch/otp-atlas/checks/CategoryUsage.php" | mailx -s "Category Usage" -c duns@cern.ch duns@cern.ch
fi
#echo $output
