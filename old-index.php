<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
  Home &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="js/search.min.js" type="text/javascript"></script>
-->
<script src="js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="css/atlas.css" >
<link rel="stylesheet" href="css/otp.css" >
<script src="js/otp.js" type="text/javascript"></script>




        
        
    </head>

    <body>
        
<div class="container">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="">ATLAS OTP</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Documentation
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="docs/index.html">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="https://otp-atlas.docs.cern.ch" target="OTPUserManual">OTP User Manual</a>
            <a class="dropdown-item" href="docs/API.html">Public API</a>
            <a class="dropdown-item" href="docs/AUTHDB.html">AUTHDB Usage</a>
            <a class="dropdown-item" href="docs/Calculations.html">Calculations</a>
            <a class="dropdown-item" href="docs/FAQ.html">FAQ</a>
            <a class="dropdown-item" href="docs/OTvalues.html">OT Values</a>
            <a class="dropdown-item" href="docs/Photos.html">Photos</a>
            <a class="dropdown-item" href="docs/SignupForShifts.html">How to Signup for Shifts</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="reports/index.html">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="reports/History.php">History</a>

          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="plots/index.html">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="checks/index.html">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="checks/OnShift.php">On Shift</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="final_reports/index.php">Final Reports</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://atlas-otp.cern.ch/" target="otp-tool">OTP Tool...</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Admin
          </a>
          <div class="dropdown-menu">
            <div class="dropdown-divider">Info</div>
            <a class="dropdown-item" href="https://twiki.cern.ch/twiki/bin/view/Atlas/OTP">OTP Twiki...(redirect)</a>
            <div class="dropdown-divider">Development</div>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp/otp-www/-/milestones">OTP Report Milestones</a>
            <a class="dropdown-item" href="https://gitlab.cern.ch/atlas-otp">OTP Gitlab Repositories</a>
            <a class="dropdown-item" href="https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do">E-groups atlas-otp</a>
            <a class="dropdown-item" href="https://cern.service-now.com">Service Now</a>
            <div class="dropdown-divider">Apps</div>
            <a class="dropdown-item" href="https://atlas-otp.cern.ch/">Prod OTP Tool</a>
            <a class="dropdown-item" href="https://preprodatlas-otp.cern.ch/">Beta OTP Tool</a>
            <a class="dropdown-item" href="https://testatlas-otp.cern.ch/">Test OTP Tool</a>
            <a class="dropdown-item" href="https://devatlas-otp.cern.ch/">Dev OTP Tool</a>
            <a class="dropdown-item" href="https://atlasop.cern.ch/otp/PhoneList.php">Public Phonebook Page</a>
            <div class="dropdown-divider">Archive</div>
            <a class="dropdown-item" href="https://espace.cern.ch/atlas-otp/Shared%20Documents/reporting.aspx?PageView=Shared">Sharepoint Reports</a>
          </div>
        </li>

      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <!-- Nav tabs -->
<ul class="nav nav-tabs" id="otpTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" id="reports-tab" data-toggle="tab" href="#reports" role="tab" aria-controls="reports" aria-selected="true">Reports</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="reports-dev-tab" data-toggle="tab" href="#reports-dev" role="tab" aria-controls="reports-dev" aria-selected="true">Reports under Development</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="checks-tab" data-toggle="tab" href="#checks" role="tab" aria-controls="checks" aria-selected="false">Checks</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="tests-tab" data-toggle="tab" href="#tests" role="tab" aria-controls="tests" aria-selected="false">Tests</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">

  <!-- HOME -->
  <div class="tab-pane" id="home" role="tabpanel" aria-labelledby="home-tab">
  </div>

  <!-- REPORTS -->
  <div class="tab-pane active" id="reports" role="tabpanel" aria-labelledby="reports-tab">
    <div class="card-deck">

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/Allocation.php" class="btn btn-primary">Allocation</a>
          </p>
          <p class="card-text">Shows allocations for people by year and class.</p>
        </div>
        <img class="card-img-bottom" src="img/300/Allocation.png" alt="Allocation Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/History.php" class="btn btn-primary">History</a>
          </p>
          <p class="card-text">Shows (un-)booking history of a particular task/requirement.</p>
        </div>
        <img class="card-img-bottom" src="img/300/History.png" alt="History Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/Requirement.php" class="btn btn-primary">Requirement</a>
          </p>
          <p class="card-text">Shows requirements for tasks by year and class.</p>
        </div>
        <img class="card-img-bottom" src="img/300/Requirement.png" alt="Requirement Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/MultiYear.php" class="btn btn-primary">Multi-Year</a>
            <a href="https://atlasop.cern.ch/otp/MultiYear.php?byInstitution" class="btn btn-primary">MY by Inst.</a>
          </p>
          <p class="card-text">Shows allocations and requirements over the past years by Funding Agencies and Institions.</p>
        </div>
        <img class="card-img-bottom" src="img/300/MultiYear.png" alt="Multi-Year Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/Commitment.php" class="btn btn-primary">Commitment</a>
          </p>
          <p class="card-text">Shows institutional commitments by year and class.</p>
        </div>
        <img class="card-img-bottom" src="img/300/Commitment.png" alt="Commitment Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/CommitmentFunding.php" class="btn btn-primary">Comm. Funding</a>
            <a href="https://atlasop.cern.ch/otp/CommitmentFunding.php?byInstitution" class="btn btn-primary">CF by Inst.</a>
          </p>
          <p class="card-text">Shows institutional commitments by Fundung Agency.</p>
        </div>
        <img class="card-img-bottom" src="img/300/CommitmentFunding.png" alt="Commitment Funding Report">
      </div>

    </div>



    <div class="card-deck">

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/FundingAgency.php" class="btn btn-primary">Funding Agency</a>
            <a href="https://atlasop.cern.ch/otp/FundingAgency.php?byInstitution" class="btn btn-primary">FA by Inst.</a>
          </p>
          <p class="card-text">Funding Agency Report showing FTE or shifts by month and institute.</p>
        </div>
        <img class="card-img-bottom" src="img/300/FundingAgency.png" alt="Funding Agency Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/Institution.php" class="btn btn-primary">Institution</a>
          </p>
          <p class="card-text">Institution Report showing FTE or shifts by month and institute.</p>
        </div>
        <img class="card-img-bottom" src="img/300/Institution.png" alt="Institution Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/Task.php" class="btn btn-primary">Task</a>
          </p>
          <p class="card-text">Task Report showing allocations and requirements by task for a full year.</p>
        </div>
        <img class="card-img-bottom" src="img/300/Task.png" alt="Task Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/TaskSchedule.php" class="btn btn-primary">Task Schedule</a>
          </p>
          <p class="card-text">Task Schedule Report showing allocations for particular tasks.</p>
        </div>
        <img class="card-img-bottom" src="img/300/TaskSchedule.png" alt="Task Schedule Report">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/PersonSchedule.php" class="btn btn-primary">Person Schedule</a>
          </p>
          <p class="card-text">Person Schedule Report showing allocations for a particular person.</p>
        </div>
        <img class="card-img-bottom" src="img/300/PersonSchedule.png" alt="Person Schedule Report">
      </div>

      <div class="card">
      </div>

    </div>

  </div>

  <!-- REPORTS Development-->
  <div class="tab-pane" id="reports-dev" role="tabpanel" aria-labelledby="reports-dev-tab">
    <div class="card-deck">

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="reports/FundingAgency.php" class="btn btn-primary">Funding Agency</a>
            <a href="reports/FundingAgency.php?byInstitution" class="btn btn-primary">FA by Inst.</a>
          </p>
          <p class="card-text">Funding Agency Report showing FTE or shifts by month and institute.</p>
        </div>
        <img class="card-img-bottom" src="img/300/FundingAgency.png" alt="Funding Agency Report">
      </div>

      <div class="card">
      </div>

      <div class="card">
      </div>

      <div class="card">
      </div>

      <div class="card">
      </div>

      <div class="card">
      </div>

    </div>
  </div>



  <!-- CHECKS -->
  <div class="tab-pane" id="checks" role="tabpanel" aria-labelledby="checks-tab">
    <div class="card-deck">

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/CategoryUsage.php" class="btn btn-primary">Category Usage Check</a>
          </p>
          <p class="card-text">Find tasks using the old category codes.</p>
        </div>
        <img class="card-img-bottom" src="img/300/CategoryUsage.png" alt="Category Usage Check">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/ConflictingShifts.php" class="btn btn-primary">Conflicting Shifts Check</a>
          </p>
          <p class="card-text">Find people doing shifts too close too eachother.</p>
        </div>
        <img class="card-img-bottom" src="img/300/ConflictingShifts.png" alt="Conflicting Shifts Check">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/DoubleDesks.php" class="btn btn-primary">Double Desks Check</a>
          </p>
          <p class="card-text">Find people doing two shifts at the same time.</p>
        </div>
        <img class="card-img-bottom" src="img/300/DoubleDesks.png" alt="Double Desks Check">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/OnShift.php" class="btn btn-primary">On Shift Check</a>
          </p>
          <p class="card-text">Find if people are trained for shifts.</p>
        </div>
        <img class="card-img-bottom" src="img/300/OnShift.png" alt="On Shift Check">
      </div>

      <div class="card">
      </div>

      <div class="card">
      </div>

    </div>
  </div>

  <!-- TESTS -->
  <div class="tab-pane" id="tests" role="tabpanel" aria-labelledby="tests-tab">
    <div class="card-deck">

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://datatables.net/media/blog/2017-12-31/dynamic.html" class="btn btn-primary">Original</a>
            <a href="tests/ScrollTest.html" class="btn btn-primary">Scroll Test</a>
          </p>
          <p class="card-text">Scroll Test with ajax data and vertical scroll fitting.</p>
        </div>
        <img class="card-img-bottom" src="img/300/ScrollTest.png" alt="Scroll Test">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="tests/BootstrapSelectTest.html" class="btn btn-primary">Original</a>
            <a href="tests/OtpBootstrapSelectTest.php" class="btn btn-primary">Bootstrap Select Test</a>
          </p>
          <p class="card-text">Bootstrap Select tests for Bootstrap 4.</p>
        </div>
        <img class="card-img-bottom" src="" alt="">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/PublicPhoneListRedirect.html" class="btn btn-primary">Redirect Test</a>
          </p>
          <p class="card-text">Checks if the Public PhoneList redirect works.</p>
        </div>
        <img class="card-img-bottom" src="" alt="">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/UnicodeTest.php" class="btn btn-primary">Unicode Test</a>
          </p>
          <p class="card-text">Not Working Yet.</p>
        </div>
        <img class="card-img-bottom" src="" alt="">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/GetClientAddress.php" class="btn btn-primary">Client Address Test</a>
          </p>
          <p class="card-text">Tests from which client (ip) the request comes.</p>
        </div>
        <img class="card-img-bottom" src="" alt="">
      </div>

      <div class="card">
        <div class="card-body">
          <p class="card-text">
            <a href="https://atlasop.cern.ch/otp/TrainingIndico.php" class="btn btn-primary">Training Indico Test</a>
          </p>
          <p class="card-text">Not Working Yet.</p>
        </div>
        <img class="card-img-bottom" src="" alt="">
      </div>

      <div class="card">
      </div>

    </div>
  </div>
</div>


  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer"><a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-www/pipelines">Version: 5.1.6-8-gd76cea8
</a><li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-www-atlas-alpha.web.cern.ch">Goto Staged Website</a><li>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2024 CERN</li>
          <li class="nav-item"><a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Website"><i class="fa fa-envelope"></i>&nbsp;ATLAS OTP</a></li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
