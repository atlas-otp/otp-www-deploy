$(document).ready(function() {
  "use strict";
  otpInitBegin("Occupancy");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('institute', 'Institutes');
});
