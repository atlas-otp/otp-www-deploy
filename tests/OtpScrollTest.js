$(document).ready( function () {
  // DataTables initialisation
  var table = $('#example').DataTable( {
    scrollResize: true,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    paging: false,
    lengthChange: false,
    // deferRender: true,
    // scroller: true,
  });
} );
