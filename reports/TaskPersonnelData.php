<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>

<?php

// NOTE, in FTE, factor applied
$estimates = array(
  2024 => array(
    "Class 1" => 7267/365,
    "Class 2" => 24832/365,
    "Class 3" => 691,
    "Class 4" => 0,
    "Upgrade Construction" => 918
  ),
  2023 => array(
    "Class 1" => 6825/365,
    "Class 2" => 22766/365,
    "Class 3" => 714,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2022 => array(
    "Class 1" => 7487/365,
    "Class 2" => 22774/365,
    "Class 3" => 704,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2021 => array(
    "Class 1" => 1194/365,
    "Class 2" => 10906/365,
    "Class 3" => 671,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2020 => array(
    "Class 1" => 1693/365,
    "Class 2" => 10402/365,
    "Class 3" => 656,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2019 => array(
    "Class 1" => 1294/365,
    "Class 2" => 11334/365,
    "Class 3" => 686,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2018 => array(
    "Class 1" => 7442/365,
    "Class 2" => 27727/365,
    "Class 3" => 696,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2017 => array(
    "Class 1" => 6082/365,
    "Class 2" => 27653/365,
    "Class 3" => 666,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2016 => array(
    "Class 1" => 6799/365,
    "Class 2" => 32496/365,
    "Class 3" => 709,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2015 => array(
    "Class 1" => 7648/365,
    "Class 2" => 27285/365,
    "Class 3" => 728,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2014 => array(
    "Class 1" => 0/365,
    "Class 2" => 0/365,
    "Class 3" => 710,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2013 => array(
    "Class 1" => 0/365,
    "Class 2" => 0/365,
    "Class 3" => 708,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2012 => array(
    "Class 1" => 8440/365,
    "Class 2" => 46200/365,
    "Class 3" => 650,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2011 => array(
    "Class 1" => 11200/365,
    "Class 2" => 46200/365,
    "Class 3" => 650,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
  2010 => array(
    "Class 1" => 16900/365,
    "Class 2" => 46206/365,
    "Class 3" => 650,
    "Class 4" => 0,
    "Upgrade Construction" => 0
  ),
);

// NOTE: defaults need to be set when including this file
$defaultYear = date("Y");   // current year
// $defaultStart = "01-Jan-" . date('y');  // start of current year
$defaultStart = date('d-M-Y');
$defaultEnd = "31-Dec-" . date('Y'); // end of current year
$defaultWeeks = "" != "" ? "" : 4;
$defaultCategory = "" != "" ? "" : "Class 1";
$defaultFunding = "" != "" ? "" : "All";
$defaultInstitution = "" != "" ? "" : "All";
$defaultSystem = "" != "" ? "" : "All";
$defaultActivity = "" != "" ? "" : "All";
$defaultType = "" != "" ? "" : "All";
$defaultRecognition = "" != "" ? "" : "Duty";
$defaultTask = "" != "" ? "" : 0;
$defaultPerson = "" != "" ? "" : 0;
$defaultShadowTask = "" != "" ? "" : 0;
$defaultMinShadows = "" != "" ? "" : 1;
$defaultTrainingTask = "" != "" ? "" : 0;
$defaultMinTrainings = "" != "" ? "" : 1;
$defaultRequirement = "" != "" ? "" : 0;
$defaultName = "" != "" ? "" : 'First L.';

$otpDebug = false;
$otpConnection = null;
$otpDatabase = '';
$otpStatementId = null;
$otpSql = '';
$otpShowSql = false;

function otpSetup() {
  global $otpDebug, $otpShowSql;

  if (isset($_REQUEST['debug'])) {
    $otpDebug = true;
  } else {
    $otpDebug = false;
  }

  if (isset($_REQUEST['sql'])) {
    $otpShowSql = true;
  } else {
    $otpShowSql = false;
  }

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  date_default_timezone_set("Europe/Zurich");
}

function otpGetRequestValue($key, $defaultValue) {
  // NOTE: Using isset() rather than empty(), to make sure 'category=' results in '' rather than 'All'
  return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $defaultValue;
}

function otpDate($dateString) {
  $dateString = str_replace('"','',$dateString);
  $dateString = str_replace("'",'',$dateString);
  $date = date_create_from_format('d-M-y', $dateString);    // 1-Dec-20 1-December-20 01-Dec-20 01-December-20
  if (!$date) {
    $date = date_create_from_format('d-M-Y', $dateString);  // 1-Dec-2020 1-December-2020 01-Dec-2020 01-December-2020
  }
  if (!$date) {
    $date = date_create_from_format('d-m-y', $dateString);  // 1-1-20 1-01-20 1-12-20
  }
  if (!$date) {
    $date = date_create_from_format('d-m-Y', $dateString);  // 1-1-2020 1-01-2020 1-12-2020
  }
  return $date;
}

function otpDateId($dateString) {
  $date = otpDate($dateString);

  if (!$date) {
    return "197010101";
  }
  $quarter = floor((date_format($date, 'm')-1)/3) + 1;
  return date_format($date, 'Y'.$quarter.'md');
}

function otpDateString($dateString) {
  $date = otpDate($dateString);

  if (!$date) {
    return "'1970-01-01'";
  }
  return "'" . date_format($date, 'Y-m-d') . "'";
}

function otpDateDt($dateString, $isMySql) {
  $date = otpDateString($dateString);
  return $isMySql ? "str_to_date($date, '%Y-%m-%d')" : "to_date($date, 'YYYY-MM-DD')";
}

function otpParams() {
  global $defaultYear, $defaultStart, $defaultEnd, $defaultWeeks;
  global $defaultCategory, $defaultSystem, $defaultActivity;
  global $defaultType, $defaultRecognition;
  global $defaultFunding, $defaultInstitution;
  global $defaultPerson;
  global $defaultTask, $defaultShadowTask, $defaultTrainingTask;
  global $defaultMinShadows, $defaultMinTrainings;
  global $defaultRequirement;
  global $defaultName;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $count, $not_ok;

  global $sql_details, $isMySql;

  $isMySql = $sql_details['type'] == 'Mysql';

  // December
  global $DEC;
  $DEC = $isMySql ? '`DEC`' : '"DEC"';
  // NVL
  global $NVL;
  $NVL = $isMySql ? 'IFNULL' : 'NVL';
  // LOAD
  global $LOAD;
  $LOAD = $isMySql ? '`LOAD`' : '"LOAD"';
  // SYSTEM
  global $SYSTEM;
  $SYSTEM = $isMySql ? '`SYSTEM`' : '"SYSTEM"';
  // DESCRIPTION
  global $DESCRIPTION;
  $DESCRIPTION = $isMySql ? '`DESCRIPTION`' : '"DESCRIPTION"';
  // LISTAGG
  global $LISTAGG;
  $LISTAGG = $isMySql ? 'GROUP_CONCAT' : 'LISTAGG';

  $dateFormat = 'd-M-Y';
  $now = date($dateFormat);
  $weekInSeconds = 60 * 60 * 24 * 7;

  $year = otpGetRequestValue('year', $defaultYear);
  $defaultStart = "01-Jan-" . $year; // start of current year
  $defaultEnd = "01-Jan-" . ($year+1); // start of next year
  $start = otpGetRequestValue('start', $defaultStart);
  $end = otpGetRequestValue('end', $defaultEnd);
  $weeks = otpGetRequestValue('weeks', $defaultWeeks);
  $category = otpGetRequestValue('category', $defaultCategory);
  $funding = otpGetRequestValue('funding', $defaultFunding);
  $institution = otpGetRequestValue('institution', $defaultInstitution);
  $system = otpGetRequestValue('system', $defaultSystem);
  $activity = otpGetRequestValue('activity', $defaultActivity);
  $type = otpGetRequestValue('type', $defaultType);
  $recognition = otpGetRequestValue('recognition', $defaultRecognition);
  $person = otpGetRequestValue('person', $defaultPerson);
  $task = otpGetRequestValue('task', $defaultTask);
  $shadowTask = otpGetRequestValue('shadowTask', $defaultShadowTask);
  $minShadows = otpGetRequestValue('minShadows', $defaultMinShadows);
  $trainingTask = otpGetRequestValue('trainingTask', $defaultTrainingTask);
  $minTrainings = otpGetRequestValue('minTrainings', $defaultMinTrainings);
  $requirement = otpGetRequestValue('requirement', $defaultRequirement);
  $name = otpGetRequestValue('name', $defaultName);

  // FLAG
  if (isset($_REQUEST['commingWeeks'])) {
    $start = $now;
    $end = date($dateFormat, strtotime("now") + ($numberOfWeeks * $weekInSeconds));
  }

  // date_id
  $startId = otpDateId($start);
  $endId = otpDateId($end);
  $nowId = otpDateId($now);

  // convert to DB
  $startDt = otpDateDt($start, $isMySql);
  $endDt = otpDateDt($end, $isMySql);
  $nowDt = otpDateDt($now, $isMySql);
}

include( "config.php" );
include ("editor/lib/DataTables.php");

use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions,
    DataTables\Database,
       DataTables\Database\Query,
       DataTables\Database\Result;

function otpConnect() {
  global $sql_details, $db, $person;

  // Get logged in Person
  if ($person == 0) {
    if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost', 'otp-www.localhost'])) {
      $user = "DUNS";
    } else {
      // Old SSO
      $user = getenv("OIDC_CLAIM_cern_upn");
      if (!$user) {
          // compatible with OKD4 SSO
          $user = $_SERVER['HTTP_X_FORWARDED_USER'];
      }

      if ($user) {
          $user = strtoupper($user);
      } else {
          $user = "";
      }
    }

    $sql = "select ID  from PUB_PERSON where USERNAME = '" . $user . "'";

    $result = $db->sql( $sql );

    if ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
      $person = $row['ID'];
    }
  }

}

function otpSqlStart() {
  global $otpSql, $db, $otpResult;

  $otpResult = $db->sql( $otpSql );
}

function otpToJson() {
  global $otpResult, $db, $otpSql;
  global $isMySql;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $name;
  global $count, $not_ok;

  // NOTE: php will have undefined values as not all times these are available
  echo "  \"year\": ", $year, ",\n";
  echo "  \"start\": \"", $start, "\",\n";
  echo "  \"start_dt\": \"", $startDt, "\",\n";
  echo "  \"start_id\": \"", $startId, "\",\n";
  echo "  \"end\": \"", $end, "\",\n";
  echo "  \"end_dt\": \"", $endDt, "\",\n";
  echo "  \"end_id\": \"", $endId, "\",\n";
  echo "  \"weeks\": ", $weeks, ",\n";
  echo "  \"now\": \"", $now, "\",\n";
  echo "  \"now_dt\": \"", $nowDt, "\",\n";
  echo "  \"now_id\": \"", $nowId, "\",\n";
  echo "  \"category\": \"", $category, "\",\n";
  echo "  \"system\": \"", $system, "\",\n";
  echo "  \"activity\": \"", $activity, "\",\n";
  echo "  \"type\": \"", $type, "\",\n";
  echo "  \"recognition\": \"", $recognition, "\",\n";
  echo "  \"funding\": \"", $funding, "\",\n";
  echo "  \"institution\": \"", $institution, "\",\n";
  echo "  \"task\": \"", $task, "\",\n";
  echo "  \"person\": \"", $person, "\",\n";
  echo "  \"shadowTask\": \"", $shadowTask, "\",\n";
  echo "  \"minShadows\": \"", $minShadows, "\",\n";
  echo "  \"trainingTask\": \"", $trainingTask, "\",\n";
  echo "  \"minTrainings\": \"", $minTrainings, "\",\n";
  echo "  \"requirement\": \"", $requirement, "\",\n";
  echo "  \"name\": \"", $name, "\",\n";
  echo "  \"is_mysql\": \"", $isMySql, "\",\n";
  // echo "  \"version\": \"", exec('git describe --tags --always'), "\",\n";
  echo "  \"data\": [\n";

  $first = true;

  $count = 0;
  $not_ok = 0;

  while ($row = $otpResult->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    // see http://php.net/manual/en/function.json-encode.php, UTF-8-encode, otherwise we will loose names with accents
    array_walk_recursive($row, function(&$val) {
      $val = utf8_encode($val);
    });

    echo ($first ? "    " : ",\n    ").json_encode($row);
    $first = false;
    $count++;
    if (array_key_exists('OK', $row) && ($row['OK'] != 'Yes')) {
      $not_ok++;
    }
  }

  echo "\n  ],\n";
  echo "  \"count\": ", $count, ",\n";
  echo "  \"not_ok\": ", $not_ok, "\n";
}

function otpSqlEnd() {
}

function otpClose() {
  global $count, $not_ok;

  if (isset($_REQUEST['count'])) {
    exit($count);
  }
  if (isset($_REQUEST['not_ok'])) {
    exit($not_ok);
  }
}

function otpHeader() {
  global $otpDatabase, $otpShowSql;

  if ($otpShowSql) {
    header("Content-Type: text/plain");
  } else {
    header("Content-Type: application/json");
    header("Oracle-DB: ".$otpDatabase);
  }
}

function otpShowSql() {
  global $otpSql;

  echo $otpSql.";\n";
}

function otpQuote($string) {
  // split by comma (separator) except if followed by whitespace (e.g. Yerevan,NRC KI, Protvino,NIKHEF => Yerevan; NRC KI, Protvino; NIKHEF), issue #118
  $string = "'" . preg_replace('/,(?!\s)/m', "','", $string) . "'";
  return $string;
}

function getLabel($name) {
  if ($name == 'funding') return "Funding Agency";
  if ($name == 'name') return "Display Name";

  return ucwords($name);
}

function getOptions($name, $sql = '') {
  global $db;

  if ($name == 'category') {
    echo "<option>Class 1</option>\n";
    echo "<option>Class 2</option>\n";
    echo "<option>Class 3</option>\n";
    echo "<option>Class 4</option>\n";
    echo "<option>Upgrade Construction</option>\n";
    return;
  }

  if ($name == 'year') {
    for ($year = 2024; $year >= 2010; $year--) {
      echo "<option>$year</option>\n";
    }
    return;
  }

  if ($name == 'type') {
    echo "<option>Expert</option>\n";
    echo "<option>Shifter</option>\n";
    return;
  }

  if ($name == 'recognition') {
    echo "<option>Duty</option>\n";
    echo "<option>Personal Duty</option>\n";
    echo "<option>Contract</option>\n";
    echo "<option>Not Operational Task</option>\n";
    return;
  }

  if ($name == 'unit') {
    echo "<option>Auto Units</option>\n";
    echo "<option>Shifts</option>\n";
    echo "<option>FTEs</option>\n";
    return;
  }

  if ($name == 'name') {
    echo "<option>Full Name</option>\n";
    echo "<option>F. Last</option>\n";
    echo "<option>First L.</option>\n";
    return;
  }

  $result = $db->sql( $sql );

  echo '  "'.$name.'": [';
  $oldGroup = "";
  $group = "";
  while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    $group = array_key_exists('GRP', $row) ? trim($row['GRP']) : "";
    if ($group != $oldGroup) {
      if ($oldGroup != "") {
        echo "</optgroup>\n";
      }
      echo "<optgroup label=\"".$group."\">\n";
      $oldGroup = $group;
    }
    // Inlined commas will have a space behind them, so they can be distinguished from comma separators
    $value = $row['NAME']; // str_replace(',','|',$row['NAME']);
    echo "<option>".trim($value)."</option>\n";
  }
  if ($oldGroup != "") {
    echo "</optgroup>\n";
  }
}

function getButton($name) {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
  echo "<button type=\"button\" class=\"btn btn-primary mb-2 mr-sm-2\" id=\"$name\">$label</button>\n";
  echo "</div>\n";
}

function getSingleSelect($name, $sql = '') {
  getSelect($name, $sql, false);
}

function getSelect($name, $sql = '', $multiple = true, $all=false) {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\">$label</span>\n";
      echo "</div>\n";
      echo "<select ".($multiple ? "multiple=\"multiple\"" : "")." class=\"selectpicker\" id=\"$name\" data-width=\"fit\">\n";
        if ($all) {
          echo "<option disabled=\"disabled\">All</option>\n";
        }
        getOptions($name, $sql);
      echo "</select>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getInput($name) {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" placeholder=\"$label\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getCheck($name, $state) {
  $label = getLabel($name);
  echo "<div class=\"form-check\">\n";
    echo "<input class=\"form-check-input\" type=\"checkbox\" value=\"" . ($state ? "true" : "false") . "\" id=\"$name\"" . ($state ? " checked=\"checked\"" : "") . ">\n";
    echo "<label class=\"form-check-label\" for=\"$name\">$label</label>\n";
  echo "</div>\n";
}

function otpReset() {
  getButton("Reset");
}

function otpSelectDateRange() {
  global $start, $end;
  $name = "dates";
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" type=\"text\" name=\"daterange\" value=\"$start - $end\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function otpSelectTask() {
  getInput("task");
}

function otpSelectPerson() {
  getInput("person");
}

function otpSelectShadowTask() {
  getInput("shadowTask");
}

function otpSelectMinShadows() {
  getInput("minShadows");
}

function otpSelectTrainingTask() {
  getInput("trainingTask");
}

function otpSelectMinTrainings() {
  getInput("minTrainings");
}

function otpSelectRequirement() {
  getInput("requirement");
}


function otpSelectCategory($single = false, $all = false) {
  // select CODE as NAME from DOMAIN_LISTS where TYP_CODE='Category' and ALLOW_SELECT='Y' order by ORDER_IN_TYPE
  getSelect('category', '', !$single, $all);
}

function otpSelectYear($single = false, $all = false) {
  getSelect('year', '', !$single, $all);
}

function otpSelectSystem($single = false, $all = false) {
  getSelect('system', "select TITLE as NAME from PUB_SYSTEM_NODE order by TITLE", !$single, $all);
}

function otpSelectActivity($single = false, $all = false) {
  getSelect('activity', "select TITLE as NAME from PUB_WBS_NODE where PARENT_ID is null order by TITLE", !$single, $all);
}

function otpSelectType($single = false, $all = false) {
  getSelect('type', '', !$single, $all);
}

function otpSelectRecognition($single = false, $all = false) {
  getSelect('recognition', '', !$single, $all);
}

function otpSelectUnit($single = false, $all = false) {
  getSelect('unit', '', !$single, $all);
}

function otpSelectFunding($single = false, $all = false) {
  getSelect('funding', "select distinct F.NAME as NAME from PUB_FUNDING_AGENCY F join PUB_INSTITUTE I on I.FUNDA_ID = F.ID order by F.NAME", !$single, $all);
}

function otpSelectInstitution($single = false, $all = false) {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID  where I.ID < 2000 or I.ID >= 3000 order by F.NAME, ONAME", !$single, $all);
}

function otpSelectInstitutionAndICs($single = false, $all = false) {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID order by F.NAME, ONAME", !$single, $all);
}

function otpSelectName() {
  getSelect("name", '', false, false);
}

function otpShowMonths($state = false) {
  getCheck("show_months", $state);
}
?>


<?php
  global $otpDebug, $otpShowSql;

  otpSetup();

  if ($otpDebug) {
      header("Content-Type: application/json");
      echo file_get_contents("TaskPersonnelDataExample.json");
      exit();
  }

  otpParams();
  otpConnect();
  otpHeader();
  if (!$otpShowSql) { echo "{\n"; }
?>

  <?php
$yearly_factor = 12;

$lastYear = $year - 1;
$nextYear = $year + 1;

$category = otpQuote($category);
$system = otpQuote($system);
$activity = otpQuote($activity);
$type = otpQuote($type);
$recognition = otpQuote($recognition);

// Show all Requirements which has FTE in the previous or current year,
// listing all Personnel on each Requirement.
// 8 seconds to run on Muon Upgrade 2021

// we first get a constant list of RIDs (otherwise this is evaluated 50.000 times)
// -- All REQ with FTE > 0 for this year or previous year
$otpSql = "SELECT RES_REQUIREMENT_ID from (
    select RES_REQUIREMENT_ID, SUM(ALLOCATED_FTE) FTE from PUB_SUM_ALLOCATION_V SA
    join PUB_TASK T on T.ID = SA.TASK_ID
    join PUB_SYSTEM_NODE S on S.ID = T.SYSTEM_ID
    join PUB_WBS_NODE WBS on WBS.ID = T.WBS_ID
    join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
    where extract(year from SA.DT) in ($lastYear, $year)
    and (('All' in ($category)) or (T.CATEGORY_CODE in ($category)))
    and (('All' in ($recognition)) or (SA.RECOGNITION_CODE in ($recognition)))
    and (('All' in ($type)) or (SA.REQ_TYPE_CODE in ($type)))
    and (('All' in ($system)) or (trim(S.TITLE) in ($system)))
    and (('All' in ($activity)) or (trim(A.TITLE) in ($activity)))
    group by RES_REQUIREMENT_ID
) D1 where FTE > 0";

$result = $db->sql( $otpSql );

$req_ids = array();
while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    array_push($req_ids, $row['RES_REQUIREMENT_ID']);
}
$rids = implode(",", $req_ids);

$otpSql = "SELECT GRP, T.TID, TASK, T.RID, T.REQ,
T.PID, LNAME, FNAME, LI.INSTITUTE_ID IID, LI.ONAME INAME, $NVL(PY.FTE, 0) FTE_PREV, T.FTE,
JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG,SEP, OCT, NOV, $DEC
FROM (
    SELECT GRP, TID, TASK, RID, REQ,
    PID, LNAME, FNAME, SUM(FTE)/$yearly_factor as FTE,
    SUM(CASE WHEN MM = 1 THEN FTE ELSE 0 END) JAN,
    SUM(CASE WHEN MM = 2 THEN FTE ELSE 0 END) FEB,
    SUM(CASE WHEN MM = 3 THEN FTE ELSE 0 END) MAR,
    SUM(CASE WHEN MM = 4 THEN FTE ELSE 0 END) APR,
    SUM(CASE WHEN MM = 5 THEN FTE ELSE 0 END) MAY,
    SUM(CASE WHEN MM = 6 THEN FTE ELSE 0 END) JUN,
    SUM(CASE WHEN MM = 7 THEN FTE ELSE 0 END) JUL,
    SUM(CASE WHEN MM = 8 THEN FTE ELSE 0 END) AUG,
    SUM(CASE WHEN MM = 9 THEN FTE ELSE 0 END) SEP,
    SUM(CASE WHEN MM = 10 THEN FTE ELSE 0 END) OCT,
    SUM(CASE WHEN MM = 11 THEN FTE ELSE 0 END) NOV,
    SUM(CASE WHEN MM = 12 THEN FTE ELSE 0 END) $DEC
    from (
        select GRP, TID, TASK, RID, REQ, PID, LNAME, FNAME, FTE, MM
        from (
            -- Required FTE this year (personID 0)
            select WBS.TITLE GRP, T.ID TID, T.SHORT_TITLE TASK, R.ID RID, R.TITLE REQ,
            0 as PID, '' as LNAME, '' as FNAME, SUM(SR.REQUIRED_FTE)*$yearly_factor as FTE, to_number(substr(SR.DATE_ID,6,2)) MM
            from PUB_RES_REQUIREMENT R
            join PUB_TASK T on T.ID = R.TASK_ID
            join PUB_SYSTEM_NODE S on S.ID = T.SYSTEM_ID
            join PUB_WBS_NODE WBS on WBS.ID = T.WBS_ID
            join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
            join PUB_SUM_REQUIREMENT_V SR on SR.TASK_ID = R.TASK_ID and SR.RES_REQUIREMENT_ID = R.ID
            where $year <= to_number(substr(SR.DATE_ID,1,4)) and to_number(substr(SR.DATE_ID,1,4)) < $nextYear
            and (('All' in ($category)) or (T.CATEGORY_CODE in ($category)))
            and (('All' in ($system)) or (trim(S.TITLE) in ($system)))
            and (('All' in ($activity)) or (trim(A.TITLE) in ($activity)))
            group by WBS.TITLE, T.ID, T.SHORT_TITLE, R.ID, R.TITLE, to_number(substr(SR.DATE_ID,6,2))
        union all
          -- Personnel for Requirement
            select WBS.TITLE GRP, T.ID TID, T.SHORT_TITLE TASK, R.ID RID, R.TITLE REQ,
            P.ID PID, P.LNAME LNAME, P.FNAME FNAME, $NVL(CY.FTE, 0) FTE, CY.MM
            from PUB_RES_REQUIREMENT R
            join PUB_TASK T on T.ID = R.TASK_ID
            join PUB_WBS_NODE WBS on WBS.ID = T.WBS_ID
            join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
            join PUB_PERSONNEL M on M.RES_REQUIREMENT_ID = R.ID and M.TASK_ID = T.ID
            join PUB_PERSON P on P.ID = M.PERSON_ID
            left outer join (
            -- FTE for this year
                select TASK_ID, RES_REQUIREMENT_ID, ALLOCATED_PERSON_ID,
                SUM(ALLOCATED_FTE)*$yearly_factor FTE, to_number(substr(SA.DATE_ID,6,2)) MM
                from PUB_SUM_ALLOCATION_V SA
                join PUB_TASK T on T.ID = SA.TASK_ID
                join PUB_SYSTEM_NODE S on S.ID = T.SYSTEM_ID
                join PUB_WBS_NODE WBS on WBS.ID = T.WBS_ID
                join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
                where extract(year from SA.DT) = $year
                and (('All' in ($category)) or (T.CATEGORY_CODE in ($category)))
                and (('All' in ($recognition)) or (SA.RECOGNITION_CODE in ($recognition)))
                and (('All' in ($type)) or (SA.REQ_TYPE_CODE in ($type)))
                and (('All' in ($system)) or (trim(S.TITLE) in ($system)))
                and (('All' in ($activity)) or (trim(A.TITLE) in ($activity)))
                group by TASK_ID, RES_REQUIREMENT_ID, ALLOCATED_PERSON_ID, to_number(substr(SA.DATE_ID,6,2))
            ) CY on CY.TASK_ID = T.ID and CY.RES_REQUIREMENT_ID = R.ID and CY.ALLOCATED_PERSON_ID = P.ID
        ) D1
        -- All REQ with FTE > 0 for this year or previous year
        where RID in ($rids)
    ) D2
    group by GRP, TID, TASK, RID, REQ, PID, LNAME, FNAME
    order by GRP, TASK, REQ, LNAME, FNAME
) T
left join (
    select sa.task_id tid, sa.res_requirement_id rid, sa.allocated_person_id pid, sum(sa.allocated_fte) FTE
    from PUB_SUM_ALLOCATION_V SA
    where extract(year from SA.DT) = $lastYear
    group by sa.task_id, sa.res_requirement_id, sa.allocated_person_id
    union all
    select sr.task_id tid, sr.res_requirement_id rid, 0 as pid, sum(sr.required_fte) as FTE
    from PUB_SUM_REQUIREMENT_V SR
    where $lastYear <= to_number(substr(SR.DATE_ID,1,4)) and to_number(substr(SR.DATE_ID,1,4)) < $year
    group by sr.task_id, sr.res_requirement_id
) PY on PY.TID = T.TID and PY.RID = T.RID and PY.PID = T.PID
left join PUB_LAST_INSTITUTE LI
on LI.PERSON_ID = T.PID
";

?>


<?php
  if ($otpShowSql) {
    otpShowSql();
    otpClose();
    return;
  }
  otpSqlStart();
  otpToJson();
  otpSqlEnd();
  echo "}\n";
  otpClose();
?>
